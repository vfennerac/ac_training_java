import java.util.*;
    
public class PokerHand
{      
    public enum Result { TIE, WIN, LOSS } 

    PokerHand(String hand)
    {
      //final hand value
      int finalScore = 0;
      
      System.out.println("hand: " + hand);
      String aux[] = hand.split(" ");
      java.util.Arrays.sort(aux);
      System.out.println("as array: " + Arrays.toString(aux));
      
      //detect card ranking - default to 0
      int[] cardRanks = new int[14];
      cardRanks = getRanks(aux);
      System.out.println("Cards ranked by value: " + Arrays.toString(cardRanks));
      //flush check
      finalScore = isFlush(aux, cardRanks);
      System.out.println("Current Score: " + finalScore);
      

        
    }
    
    public int isFlush(String[] shand, int[] cranks) {
      int score = 0;
      
      for (int x=0; x<4; x++) {
      //System.out.println(aux[x].charAt(1));
          if (shand[x].charAt(1) != shand[x+1].charAt(1) ) {
              System.out.println("not a flush");
              return score;
          }
      }
      
      //standard score for any flush
      score = 500;
      for (int y=13; y==4; y--) {
        System.out.println(y);
        System.out.println(cranks[y]);
        if (cranks[y] == 1) {
          //update standard score
          score = 500 + y;
          //Straight check
          if(cranks[y-1] == 1 && cranks[y-2] == 1 && cranks[y-3] == 1 && cranks[y-4] == 1) {
            if (y==13) {
              score = 999; //royal
            } else {
              score = (800 + y);
            }
          }
        }
      }
      return score;
    }
    
    public int[] getRanks(String[] rhand) {
      int[] ranked = new int[14];
      for (int x=0; x < 5; x++) {
          //check first card value sorted
          // values are 2 = rank 1 to A = rank 13 or 0, according to card order
          char current = rhand[x].charAt(0);
          System.out.println("current card: " + current);
          if ("AJKQT".indexOf(current) != -1) {
            //current card is a figure
            if (current == 'A' && (x == 0 || rhand[x-1].charAt(0) == 'A' || rhand[x+1].charAt(0) == 'A')) {
              //ace high and ace combinations are more valuable
              ranked[13]++;
            } else if (current == 'K') {
              ranked[12]++;
            } else if (current == 'Q') {
              ranked[11]++;
            } else if (current == 'J') {
              ranked[10]++;
            } else if (current == 'T') {
              ranked[9]++;
            } else if (current =='A') {
              //A as 1
              ranked[0]++;
            }    
          } else {
              //numbers
              //System.out.println(current);
              ranked[Character.getNumericValue(current)-1]++;
            }
        }
      //System.out.println("Cards ranked by value: " + Arrays.toString(ranked));
      return ranked;
    }
    
    public Result compareWith(PokerHand hand) {        
        return Result.TIE;
    }
}
