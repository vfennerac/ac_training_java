import java.util.*;
    
public class PokerHand
{   

    private int finalScore;
    private int[] tieBreaker = new int[] {0, 0 , 0, 0, 0};
    
    public int[] getTieBreaker() {
      return tieBreaker;
    }
    
    //-- Populate tieBreaker Array
    public void setTieBreaker(int[] ranked) {
      int pos = 0;
      for (int i=13; i >=0; i--) {
        if (ranked[i] == 1) {
          tieBreaker[pos] = i+1;
          pos++;
        }
      }
    }
    
    //Tie checker
    public Result resolveTie(PokerHand p1, PokerHand p2) {
      for (int x=0; x <= 4; x++) {
        //System.out.println(p1.tieBreaker[x] + " vs " + p2.tieBreaker[x]);
        if (p1.tieBreaker[x] > p2.tieBreaker[x]) return Result.WIN;
        if (p1.tieBreaker[x] < p2.tieBreaker[x]) return Result.LOSS;
      }
      return Result.TIE;
    }
    
    public int getFinalScore(){
      return finalScore;
    }
    
    public enum Result { TIE, WIN, LOSS } 

    PokerHand(String hand)
    { 
      //-- show given hand, split and sort
      //System.out.println("Test Hand: " + hand);
      String aux[] = hand.split(" ");
      java.util.Arrays.sort(aux);
      //System.out.println("Ordered Hand: " + Arrays.toString(aux));
      //-- detect card ranking - from 0: low ace to 13 - high ace
      int[] cardRanks = new int[14];
      cardRanks = getRanks(aux);
      //System.out.println("Cards ranked by value: " + Arrays.toString(cardRanks));
      //-- generate tie breaker
      setTieBreaker(cardRanks);
      //System.out.println("Populated Tie Breaker: " + Arrays.toString(tieBreaker));
      
      //-- Check suits agains flushes, returns partial score and high card
      finalScore = isFlush(aux, cardRanks);
      //System.out.println("Current Score: " + finalScore);
      //-- checks for hands that win agains regular flush
      if (finalScore < 600) {
        finalScore = checkCombo(finalScore, cardRanks);
      }
      //System.out.println("Final Score: " + finalScore);
    }
    
   
    public int[] getRanks(String[] rhand) {
      int[] ranked = new int[14];
      for (int x=0; x < 5; x++) {
          //check first card value sorted
          // values are 2 = rank 1 to A = rank 13 or 0, according to card order
          char current = rhand[x].charAt(0);
          //System.out.println("current card: " + current);
          if ("AJKQT".indexOf(current) != -1) {
            //current card is a figure
            //System.out.println("current card is a figure");
            //System.out.println(rhand[x+1].charAt(0));
            if (current == 'A' ) {
              if (ranked[1] == 1 && ranked[2] == 1 && ranked[3] == 1 && ranked[4] == 1) {
                //low ace only used on lowest sequence
                ranked[0]++;
              } else {
              //ace high and ace combinations are more valuable
              //System.out.println("adding high ace");
              ranked[13]++;
              }
            } else if (current == 'K') {
              //System.out.println("adding King");
              ranked[12]++;
            } else if (current == 'Q') {
              //System.out.println("adding Queen");
              ranked[11]++;
            } else if (current == 'J') {
              //System.out.println("adding Jockey");
              ranked[10]++;
            } else if (current == 'T') {
              //System.out.println("adding Ten");
              ranked[9]++;
            } 
          } else {
              //numbers
              //System.out.println("adding to " + (Character.getNumericValue(current)-1));
              ranked[Character.getNumericValue(current)-1]++;
            }
        }
      //System.out.println("Cards ranked by value: " + Arrays.toString(ranked));
      return ranked;
    }
    
    public int isFlush(String[] shand, int[] cranks) {
      int score = 0;
      //System.out.println("--Checking for flush--");
      //System.out.println("internal cards: " + Arrays.toString(shand));
      //System.out.println("internal card values: " + Arrays.toString(cranks));
      for (int x=0; x<4; x++) {
      //System.out.println(aux[x].charAt(1));
          if (shand[x].charAt(1) != shand[x+1].charAt(1) ) {
              //System.out.println("No flush");
              return score;
          }
      }
      for (int y=13; y>=4; y--) {
        //System.out.println(y);
        //System.out.println(cranks[y]);
        if (cranks[y] == 1) {
          //Straight flush check
          if(cranks[y-1] == 1 && cranks[y-2] == 1 && cranks[y-3] == 1 && cranks[y-4] == 1) {
            //System.out.println("!!straight flush found!!");
            if (y==13) {
              return 999; //royal
            } else {
              return (800 + y + 1);
            }
        } else if(cranks[0] == 1) {
            return 500 + 14;
          } else {
            return 500 + y + 1;
          }
        }
      }
      return score;
    }
    
    //-- check agains combinations
    public int checkCombo(int currentScore, int[] cranks){
      int score = currentScore;
      for (int y=13; y>=0; y--) {
        //Straight check
        if (cranks[y] == 1 && y >= 4) {
          if(cranks[y-1] == 1 && cranks[y-2] == 1 && cranks[y-3] == 1 && cranks[y-4] == 1) {
            //System.out.println("!!straight found!!");
            if (y==13) {
              return 499; 
            } else {
              return (400 + y + 1);
            }
          }
        }
        //-- four check
        if (cranks[y] == 4) {
          return 700 + y + 1;
        }
        //-- three check
        if (cranks[y] == 3) {
          //System.out.println("found 3 at: " + y);
          //-- checks for high full house (3+2)
          for (int z=y-1; z>=0; z--){
            //System.out.println("current z: " + z);
            if (cranks[z] == 2){
              //set full house tie breaker
              //tieBreaker[0] = z + 1;
              return 600 + ((y+1)*3) + ((z+1)*2);
            }
          }
          //-- gets here if its just a regular three
          return 300 + y + 1;
        }
        //-- pair check
        if (cranks[y] == 2){
          //-- check for low full house (2+3)
          for (int z=y-1; z>=0; z--){
            if (cranks[z] == 3){
              //set full house tie breaker
              //tieBreaker[0] = y + 1;
              return 600 + ((z + 1)*3) + ((y+1)*2);
            }
            //-- check for 2-pair
            if (cranks[z] == 2){
              return 200 + y + 1;
            }
          }
          //single pair
          return 100 + y + 1;
        }
      }
      return score;
    }
    
    public Result compareWith(PokerHand hand) {
      //System.out.println("---");
      if (this.getFinalScore() < hand.getFinalScore()) return Result.LOSS;
      if (this.getFinalScore() > hand.getFinalScore()) return Result.WIN;
      //System.out.println("Solving Tie");
      return resolveTie(this, hand);
    }
}
