/*
    * Victor Fenner
    *
    * Java Training - codewars KATA
    *
 */

//Syntax

/*
    * Variables
    * Data Types
        * Primitive Data Types
        *
        * https://www.codewars.com/kata/parse-nice-int-from-char-problem/train/java
*/
        public class CharProblem {
            public static int howOld(final String herOld) {

                char aux = herOld.charAt(0);
                return Character.getNumericValue(aux);

            }
        }

/*
        * Operators
            * Arithmetic
            * Comparison
            * Assignment
            * Logical
        * Operators precedence
        *
        * https://www.codewars.com/kata/basic-mathematical-operations/train/java
*/
        public class BasicOperations
        {
            public static Integer basicMath(String op, int v1, int v2)
            {
                switch (op) {
                    case "+" :
                        return v1 + v2;
                    case "-" :
                        return v1 - v2;
                    case "*" :
                        return v1 * v2;
                    case "/" :
                        return v1 / v2;
                }
                return 0;
            }
        }


/*
        * Keywords and Expressions
*/

/*
        Code Organization
*/

/*
        Comments
*/

/*
        * Code Blocks
            * Methods in Java
            * Methods Overloading


        * Control Flow Statements
            * Switch
            * For
            * While/ do… While
        *
        * https://www.codewars.com/kata/makebackronym/train/java
*/

import java.util.*;
        import java.util.stream.*;

public class Backronym {
    private static Map<String, String> dictionary = Preload.dictionary;
    public static String makeBackronym(String acronym) {
        String aux = "";
        for(int i=0; i < acronym.length(); i++) {
            aux += dictionary.get(Character.toString(acronym.charAt(i)).toUpperCase()) + " ";
        }
        return aux.trim();
    }
}
/*
        *Collections
            *ArrayList
            *HashMap
            *Sets
            *Sorting Lists
            *Natural Ordering
            *Queues
            *Using Iterators
            *Deciding Which Collection to Use
            *
        * https://www.codewars.com/kata/sort-an-array-by-value-and-index/train/java -- partially complete, fails on duplicates, very interesting kata
        * 
*/

import java.util.Arrays;

public class Kata
{
  public static int[] sortByValueAndIndex(int[] array)
  {
    System.out.println(Arrays.toString(array));
    int lastnum = 0;
    int[] aux = new int[array.length];
    int[] result = new int[array.length];
    
    for (int i=0; i < array.length; i++) {
      aux[i] = array[i]*(i+1);
    }
    
    int[] sorted = aux.clone();
    Arrays.sort(sorted);
    System.out.println(Arrays.toString(aux));
    System.out.println(Arrays.toString(sorted));
    System.out.println("---adding----");
    for (int i=0; i < array.length; i++) {
      System.out.println(aux[i]+" / " + array[i]+" / " + lastnum);
      int position = result[Arrays.binarySearch(sorted, aux[i])];
       position++;
      
      if (aux[i] == lastnum) {
        result[(Arrays.binarySearch(sorted, aux[i])+1)] = array[i];
      } else{
        result[Arrays.binarySearch(sorted, aux[i])] = array[i];
      }
      System.out.println(Arrays.toString(result));
      lastnum = aux[i];
    }
    
    System.out.println("---end----");
    return result;
  }
}



